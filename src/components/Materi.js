const Text = props => {
    const {name, jabatan} = props
    return(
        <div>
            <h1>Hallo {name} as {jabatan}</h1>
        </div>
    ) 
}

// withclass
// render() {
//     const {name, position} = this.props
//     return <h1>Welcome {name} as {position}</h1> 
// }