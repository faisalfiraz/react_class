import React, { Component } from 'react';

class EventBind extends Component {
    constructor(props) {
        super(props)

        this.state = {
            message: 'Selamat Datang'
        }
        // this.ClickEvent = this.ClickEvent.bind(this)
    }
    ClickEvent(){
        this.setState({
            message: 'Selamat Jalan'
        })
        console.log(this)

    }


    render() {
        return (
            <div>
                <div>{this.state.message}</div>
                <button onClick={this.ClickEvent.bind(this)}>Click</button>
            </div>
        );
    }
}

export default EventBind;