import React, { Component } from 'react';

class ClassClick extends Component {
    clickHandler() {
        console.log("event click")
    }
    render() {
        return (
            <div>
                <button onClick={this.clickHandler}>Event Class Click</button>
            </div>
        );
    }
}

export default ClassClick;