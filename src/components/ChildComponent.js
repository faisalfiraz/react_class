import React, { Component } from 'react';

function ChildComponent(props) {
    return (
        <div>
            <button onClick={props.parentHandler}>Parent</button>
        </div>
    )
}

export default ChildComponent;