import React, { Component } from 'react';
import ChildComponent from './ChildComponent';

class ParentComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {
            parentName: 'Parent'
        }
        this.classParent = this.classParent.bind(this)
    }

    classParent() {
        alert(`Hallo ${this.state.parentName}`)
    }

    render() {
        return (
            <div>
                <ChildComponent parentHandler={this.classParent}  />
            </div>
        );
    }
}

export default ParentComponent;