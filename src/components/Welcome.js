import React, {Component} from 'react'

class Welcome extends Component {
    render() {
        const {name, jabatan} = this.props
        return <h1>Welcome {name} as {jabatan}</h1>
    }
}
export default Welcome