import React, { Component } from 'react'

class Form extends Component {
    constructor(props) {
        super(props) 

        this.state = {
            username: ''
        }
    }
    handleUsernameChange = (event) => {
        console.log(event)
        this.setState({
            username: event.target.value
        })
    }

    handleCommentChange = (event) => {
        this.setState({
            comment: event.target.value
        })
    }
    handleTopikChange = (event) => {
        this.setState({
            topic: event.target.value
        })
    }
    render(){
        return (
            <form>
                <div>
                    <label>Username</label>
                    <input type='text' value={this.state.username} onChange={this.handleUsernameChange}></input>
                </div>
                <div>
                    <label>Comments</label>
                    <textarea type='text' value={this.state.comment} onChange={this.handleCommentChange}></textarea>
                </div>
                <div>
                    <label>Topik</label>
                    <select value={this.state.topic} onChange={this.handleTopikChange}>
                        <option value='react'>React</option>
                        <option value='angular'>Angular</option>
                        <option value='vue'>Vue</option>
                    </select>
                </div>
            </form>
        )
    }
}
export default Form