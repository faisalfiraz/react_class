import logo from './logo.svg';
import './App.css';
import Text from './components/Text';
import Welcome from './components/Welcome';
import Message from './components/Message';
import Counter from './components/Counter';
import FunctionClick from './components/FunctionalClick';
import ClassClick from './components/ClassClick';
import EventBind from './components/EventBind';
import Form from './components/Form';
// import ParentComponent from './components/ParentComponent';



function App() {
  return (
    <div className="App">
      <Form />
      {/* <ParentComponent/> */}
      
      {/* <EventBind /> */}
      {/* <FunctionClick></FunctionClick>
      <ClassClick /> */}
      {/* <Counter></Counter> */}
      {/* <Text name="foo" jabatan="DevOps"></Text> */}
      {/* <Welcome name="faisal" jabatan="DevOps"></Welcome> */}

      {/* <Text name="faisal" jabatan="DevOps"></Text>
      <button>Actions</button>
      <Welcome name="faisal"></Welcome> */}
    </div>
  );
}

export default App;
